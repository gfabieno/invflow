InvFLow
--------

Library for geophysical inversion based on Tensorflow.

See the documentation in ./docs/_build/index.html. You may have to build it. to do so::

  cd ./docs/
  make html
