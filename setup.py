
from setuptools import setup

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='InvFlow',
      version='0.1',
      description='Package for geophysical inversion with Tensorflow',
      long_description=readme(),
      author='Gabriel Fabien-Ouellet',
      author_email='gfabieno@stanford.ca',
      license='MIT',
      packages=['InvFlow'],
      install_requires=[
                        'numpy',
                        'hdf5storage',
                        'h5py',
                        'scipy'
                        ],
      zip_safe=False)
