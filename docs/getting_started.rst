.. _getting_started:


***************
Getting started
***************

.. _installing-docdir:

Installing ginv
=============================

You can install Ginv package by doing in the ginv directory::

  pip install .

The installation should check for all required packages and install them automaticaly.


Testing the inversion
=============================

A test example is provided in :file:`./ginv/tests`. This package requires Python 3. Before running the example, you should make sure that SeisCL is installed on your machine. The file :file:`.ginv/tests/ssh_hosts` should be modified according to your system. This file is used to list all available machines that can be accessed with password-less ssh. Its structure is::

  username hostname option_name:option

In particular, the option path_to_prog should be set to the path where SeisCL is installed. After that, the test can be launched with::

  python test_inv.py

This should compute the data for the true model and perform 15 iterations of stochastic gradient descent. The output of the inversion is located in :file:`SGD_iter.mat`. For example, after an inversion run, we obtain

.. image:: _static/getting_started.png

