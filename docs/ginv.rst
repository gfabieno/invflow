ginv package
============
Submodules
----------

ginv.Gridcompute module
-----------------------

.. automodule:: ginv.Gridcompute
    :members:
    :undoc-members:
    :show-inheritance:

ginv.SGD module
---------------

.. automodule:: ginv.SGD
    :members:
    :undoc-members:
    :show-inheritance:

ginv.SeisCL module
------------------

.. automodule:: ginv.SeisCL
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: ginv
    :members:
    :undoc-members:
    :show-inheritance:
