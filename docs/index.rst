.. Ginv_doc documentation master file, created by
   sphinx-quickstart on Wed Jan 25 15:54:07 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Ginv_doc's documentation!
====================================

Contents:

.. toctree::
   :maxdepth: 2

   getting_started 	
   ginv


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

