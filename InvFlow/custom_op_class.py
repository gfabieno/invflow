#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import tensorflow as tf
from tensorflow.python.framework import ops
import numpy as np
import subprocess
from Forward import FclassError
import copy
import os
from shutil import copyfile,rmtree
import pdb


        
class ForwardAdjoint:
    
            
    def __init__(self,F, dataid, m, name=None):
        self.F=copy.copy(F)
        self.dataid=dataid
        self.m=m
        self.name=name
        self._op=None
        self.workdir='./scratch'+''.join([str(k) for k in np.random.randint(0,9,15) ])+'/'    
           
                
    @property
    def workdir(self):
        return self.__workdir

    @workdir.setter
    def workdir(self, workdir):
        if hasattr(self,'workdir') and os.path.exists(self.workdir):
            rmtree(self.workdir)
        self.__workdir=workdir
        if workdir and not os.path.exists(workdir):
            os.makedirs(workdir)  

    @property    
    def op(self):
        if not self._op:
            
            #No reference to outside objects are permitted in custom op functions _Forward and _Adjoint
            #Copy here the reference to avoid this
            workdir=self.workdir
            set_job=self.F.set_job
            callcmd=self.F.callcmd(self.workdir)
            input_residuals=self.F.input_residuals
            load_data=self.F.load_data
            read_rms=self.F.read_rms
            write_residuals=self.F.write_residuals
            load_grad=self.F.load_grad
            
            
            # Actual forward:
            def _Forward( dataid, m, param_names):
                success=False
                n=0
                #Try to launch the forward code, ignoring forward errors n times, else we raise an error
                while not success and n<10:
                    try:
                        names=[name.decode('ascii') for name in param_names]
                        set_job(dataid, dict(zip(names, m)) , workdir)
                        pipes = subprocess.Popen(callcmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
                        date, std_err = pipes.communicate()
                        if std_err:
                            print(std_err.decode())
                            raise tf.errors.FailedPreconditionError(message=std_err.decode())
        
                        if input_residuals:
                            output=np.float32(load_data(workdir))
                        else:
                            output=np.float32(read_rms(workdir))
                        success=True
                        n+=1
                    except (FclassError, OSError) as msg:
                        pass
                if not success:
                    raise tf.errors.FailedPreconditionError(msg)
                    
                return output
                    
            # Actual gradient:
            def _Adjoint_fun( m, param_names, residuals ):
                
                try:
                    names=[name.decode('ascii') for name in param_names]
                    if input_residuals:
                        write_residuals(residuals)
                        pipes = subprocess.Popen(callcmd(workdir),stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
                        date, std_err = pipes.communicate()
                        if std_err:
                            print(std_err.decode())
                            raise tf.errors.FailedPreconditionError(message=std_err.decode())
                    output= np.array( load_grad(workdir, names), dtype=np.float64 ) #np.array( self.F.load_grad(), dtype=m.dtype.as_numpy_dtype )
                except (FclassError, OSError) as msg:
                    raise tf.errors.FailedPreconditionError(msg)
                return  output   
            
            #The gradient function can only input and output tensor objects. To evaluate the gradient and output an numpy array, 
            #we must still create a new py_func containing the gradient function. When building graph, function in py_fun are not
            #evaluated, whereas the grad function of tf.RegisterGradient is. The latter is problematic because when initalizing the graph, we 
            #not data is transmited and subprocess calls or any operation on tensors value will fail.  
            def _Adjoint(op, grad ):                
                return op.inputs[0], tf.py_func(_Adjoint_fun, [op.inputs[1], op.inputs[2], grad], tf.float64 ), op.inputs[2]
            
            param_names=[t.name.split(':')[0] for t in self.m] 
            for param in self.F.params:
                if param not in param_names:
                    raise tf.errors.InvalidArgumentError(None, None,'This op require param %s to be defined in m'%param)
            # Build the Forward operator overriding the gradient with our own function
            with ops.name_scope(self.name, "Forward", [self.dataid,self.m, param_names]) as name:

                # Need to generate a unique name to avoid duplicates:
                rnd_name = 'PyFuncGrad' + str(np.random.randint(0, 1E+8))
                
                tf.RegisterGradient(rnd_name)(_Adjoint)
                with tf.get_default_graph().gradient_override_map({"PyFunc": rnd_name}):
                    self._op=tf.py_func(_Forward, [self.dataid,self.m, param_names], [tf.float32], stateful=True, name=name)
#                self._op=self._op=tf.py_func(_Forward, [self.dataid,self.m], [tf.float32], stateful=True, name=name)
        return self._op
    
    def __del__(self):
        self.workdir='' #this should delete workdir


        