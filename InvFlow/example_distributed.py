#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
        Distributed Tensorflow example.
        This is not working presently with my Inverter because of graphs
        syncrhonisation. Coordinating gradient application with
        SyncReplicasOptimizer is not possible because syncrhonisation happens
        with apply_gradient, but Inverter needs to process the accumulated gradient
        before applying it. I wrote a new version in sync_replicas_optimizer_oncomp
        but it remains to see if it is fully functional. A problem that will come
        up Inverter calls many times apply_gradient, but this overwrites queues
        in SyncReplicasOptimizer, each time it is call... Maybe the Inverter
        object will need to make copies of SyncReplicasOptimizer.



        Change the hardcoded host urls below with your own hosts.
        Run like this:

        pc-01$ ./example_distributed.py \
                    --ps_hosts=localhost:2222 \
                    --worker_hosts=localhost:2223 \
                    --job_name=ps --task_index=0

        pc-02$ ./example_distributed.py \
                    --ps_hosts=localhost:2222 \
                    --worker_hosts=localhost:2223 \
                    --job_name=worker --task_index=0

"""
from InvFlow.SeisCL import SeisCL
from InvFlow.Inverter import Inverter, EnableCL, InvertError
from InvFlow.sync_replicas_optimizer_oncomp2 import SyncReplicasOptimizer
import numpy as np
import argparse
import sys
import os
import tensorflow as tf

FLAGS = None


def main(_):

    batch_size = 3
    niter = 10
    logs_path = os.getcwd() + "/logs_dist" + FLAGS.job_name + "_" + str(FLAGS.task_index)

    """
        ________________Create the tensforflow cluster__________________________
    """
    ps_hosts = FLAGS.ps_hosts.split(",")
    worker_hosts = FLAGS.worker_hosts.split(",")

    # Create a cluster from the parameter server and worker hosts.
    cluster = tf.train.ClusterSpec({"ps": ps_hosts, "worker": worker_hosts})

    # Create and start a server for the local task.
    server = tf.train.Server(cluster,
                             job_name=FLAGS.job_name,
                             task_index=FLAGS.task_index)

    """
        ________Execution path for workers and parameter servers________________
    """
    if FLAGS.job_name == "ps":
        server.join()
    elif FLAGS.job_name == "worker":

        # Assigns ops to the local worker by default.
        with tf.device(tf.train.replica_device_setter(
                worker_device="/job:worker/task:%d" % FLAGS.task_index,
                cluster=cluster)):

            """
                ____________Set the forward problem variables___________________
            """
            F = SeisCL()
            F.csts['N'] = np.array([200, 150])
            F.csts['dt'] = 0.0009
            F.csts['dh'] = 10.
            F.csts['NT'] = int(round(0.7 / F.csts['dt']))
            F.csts['f0'] = 15.
            F.csts['fmax'] = 0
            F.csts['fmin'] = 0
            F.csts['L'] = 0
            F.csts['nab'] = 32
            F.csts['tmax'] = 0
            F.csts['seisout'] = 2
            F.csts['resout'] = 0
            F.csts['ND'] = 2
            F.csts['pref_device_type'] = 2
            F.file_din = os.getcwd() + '/data0_10.mat'

            """
                ______________Set the parameters for inversion__________________
            """
            F.input_residuals = False
            F.csts['back_prop_type'] = 2
            F.csts['gradfreqs'] = np.array([F.csts['f0']])
            F.csts['read_src'] = 1
            F.csts['rmsout'] = 1
            F.csts['gradout'] = 1
            F.csts['scale_datarms'] = False
            F.csts['freqstep'] = 1.
            F.csts['scalerms'] = 0
            F.csts['scalermsnorm'] = 0
            F.csts['scaleshot'] = 0
            F.file_datalist = os.getcwd() + '/data0_10.mat'
            F.csts['gradfreqs'] = np.array([F.csts['f0']])

            """
                ___________#Add sources and receivers positions_________________
            """
            for ii in range(0, 126, 12):
                F.add_src_pos((F.csts['nab'] + 5) * F.csts['dh'], 0,
                              (F.csts['nab'] + 5 + ii) * F.csts['dh'], ii / 12,
                              1)
                for jj in range(0, 126):
                    F.add_rec_pos(
                        (F.csts['N'][1] - F.csts['nab'] - 5) * F.csts['dh'], 0,
                        (F.csts['nab'] + 5 + jj) * F.csts['dh'], ii / 12)
            F.ricker()
            F.read_src = False

            """
                ____________________#Define the model___________________________
            """
            vp0 = np.zeros((F.csts['N'][0], F.csts['N'][1])) + 4000
            vs0 = np.zeros((F.csts['N'][0], F.csts['N'][1])) + 3000
            rho0 = np.zeros((F.csts['N'][0], F.csts['N'][1])) + 2000

            m = [tf.Variable(vp0, name='vp'),
                 tf.Variable(vs0, name='vs'),
                 tf.Variable(rho0, name='rho')
                 ]
            mtotrain = [m[0]]

            dataid = tf.placeholder(dtype=tf.int16, name='data_ids')

            """
                ____________________Set the Inverter object_____________________
            """
            with tf.name_scope('Forward'):
                thisop = F.op(dataid, m, name='Forward')
                output = tf.sqrt(thisop.op[0])

            with tf.name_scope('Costfun'):
                costfun = tf.losses.mean_squared_error(0, output, weights=1.0)

            vptrue = vp0
            vptrue[105:135, 50:100] = 3800

            opt = tf.train.GradientDescentOptimizer(10.0)

            nworker = len(worker_hosts)
            ngrad = nworker
            # This synchronizes gradient updates
            global_step = tf.train.get_or_create_global_step()
            opt = SyncReplicasOptimizer(opt,
                                           global_step=global_step,
                                                    replicas_to_aggregate=ngrad,
                                                    total_num_replicas=nworker,
                                                    use_locking=True
                                                    )
            # opt = tf.train.SyncReplicasOptimizer(opt,
            #                                      replicas_to_aggregate=ngrad,
            #                                      total_num_replicas=nworker,
            #                                      use_locking=True
            #                                      )
            # nab = F.csts['nab']
            # bnd = tf.constant(np.array([[nab + 2, -nab], [nab, -nab]]),
            #                   name='boundary')
            # bnds = [bnd for _ in mtotrain]
            # filtscale = tf.Variable(0, name='filtscale', trainable=False)
            # filtscales = [filtscale for _ in mtotrain]


            # inv = Inverter(opt, costfun, mtotrain, bnds=bnds,
            #                filtscales=filtscales, lbfgs=5,
            #                global_step=global_step)
            #inv = opt.minimize(costfun, global_step=global_step)
            gradacc = opt.compute_gradients(costfun)
            gradapp = opt.apply_gradients(gradacc)

            """
                __________Set variables to monitor during inversion_____________
            """
            tf.summary.scalar("cost", costfun)
            summary_op = tf.summary.merge_all()

        """
            _____________________Hooks to control the inversion_________________
        """
        is_chief = (FLAGS.task_index == 0)
        # The StopAtStepHook handles stopping after running given steps.
        hooks = [tf.train.StopAtStepHook(last_step=niter),
                 tf.train.SummarySaverHook(save_steps=1, summary_op=summary_op),
                 tf.train.CheckpointSaverHook(checkpoint_dir=logs_path,
                                              save_steps=1,
                                              saver=tf.train.Saver(max_to_keep=None)),
                 opt.make_session_run_hook(is_chief)]

        """
            _________________________Perform the inversion______________________
        """
        def sesscreate():
            return tf.train.MonitoredTrainingSession(checkpoint_dir=logs_path,
                                                     save_checkpoint_secs=None,
                                                     hooks=hooks,
                                                     save_summaries_steps=1,
                                                     master=server.target,
                                                     is_chief=is_chief,
                                                     )
        with EnableCL(session=sesscreate) as sess:

            # create log writer object (this will log on every machine)
            writer = tf.summary.FileWriter(logs_path,
                                           graph=tf.get_default_graph())
            writer.close()

            while not sess.should_stop():
                # print('iter:%d, err:%f, step:%f, maxvp:%d' % (
                #                        inv.global_step.eval(session=sess),
                #                        inv.costfun.eval(session=sess),
                #                        inv.step.eval(session=sess),
                #                        np.max(m[0].eval(session=sess))))

                this_batch = np.random.choice(F.src_pos_all[3, :],
                                              batch_size,
                                              replace=False)
                # try:
                #     inv.run(sess, feed_dict={dataid: this_batch})
                # except InvertError as msg:
                #     print(msg)
                # sys.stdout.flush()

                sess.run(gradapp, feed_dict={dataid: this_batch})
                #sess.run(inv, feed_dict={dataid: this_batch})


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.register("type", "bool", lambda v: v.lower() == "true")
    # Flags for defining the tf.train.ClusterSpec
    parser.add_argument(
        "--ps_hosts",
        type=str,
        default="",
        help="Comma-separated list of hostname:port pairs"
    )
    parser.add_argument(
        "--worker_hosts",
        type=str,
        default="",
        help="Comma-separated list of hostname:port pairs"
    )
    parser.add_argument(
        "--job_name",
        type=str,
        default="",
        help="One of 'ps', 'worker'"
    )
    # Flags for defining the tf.train.Server
    parser.add_argument(
        "--task_index",
        type=int,
        default=0,
        help="Index of task within the job"
    )
    FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
