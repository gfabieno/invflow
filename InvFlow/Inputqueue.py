#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 31 14:16:36 2017

@author: gabrielfabien-ouellet
"""

from multiprocessing import Process, Queue, Event, Value
from typing import List, Dict, Any
from InvFlow.Inverter import EnableCL
import os


class Counter(object):
    def __init__(self):
        self.val = Value('i', 0)

    def increment(self, n=1):
        with self.val.get_lock():
            self.val.value += n

    @property
    def value(self):
        return self.val.value

class GeneratorFunctionExample():
    def __init__(self):
        import tensorflow as tf
        tf.reset_default_graph()
        self.outvar = tf.constant([1],dtype=tf.int32, name='testvar')

    def generator_fun(self, sess):
        return sess.run(self.outvar)[0]

class DataGenerator(Process):
    def __init__(self,
                 data_q: Queue,
                 stop_event: Event,
                 gpus: list=[0],
                 generator_fun=lambda x: 1):
        super().__init__()
        self.done_q = data_q
        self.stop_event = stop_event
        self.generator_fun = generator_fun
        if not isinstance(gpus, list):
            gpus = [gpus]
        self.gpus = gpus

    def run(self):
        with EnableCL(target_gpus=self.gpus) as sess:
            while not self.stop_event.is_set():
                if not self.done_q.full():
                    try:
                        batch = self.generator_fun(sess)
                        self.done_q.put(batch)
                    except FileNotFoundError:
                        pass



class DataAggregator(Process):
    def __init__(self,
                 data_q: Queue,
                 batch_q: Queue,
                 stop_event: Event,
                 batch_size: int,
                 n_in_queue=None,
                 max_capacity=1000):
        super().__init__()
        self.pending_q = data_q
        self.done_q = batch_q
        self.stop_event = stop_event
        self.batch_size = batch_size
        self.batch = [[None]*batch_size]
        self.n_in_batch = 0
        if n_in_queue is None:
            self.n_in_queue = Counter()
        else:
            self.n_in_queue = n_in_queue
        self.nsaved = 0
        self.max_capacity = max_capacity

    def run(self):

        while not self.stop_event.is_set():
            if not self.done_q.full():
                self.batch[0][self.n_in_batch] = self.pending_q.get()
                self.n_in_batch += 1
                if self.n_in_batch == self.batch_size:
                    batch = self.batch.pop()
                    self.done_q.put(batch)
                    self.batch = [[None] * self.batch_size]
                    self.n_in_batch = 0
                    self.n_in_queue.increment()

class BatchManager:
    def __init__(self,
                 MAX_CAPACITY: int=1000,
                 batch_size: int=3,
                 gpus = [0],
                 generator_fun=[lambda x: 1]):

        if len(gpus) != len(generator_fun):
            raise ValueError('gpus and generator_fun should be lists of the '
                             'same length')
        self.stop_event = Event()
        self.data_q = Queue(MAX_CAPACITY)
        self.batch_q = Queue(MAX_CAPACITY)
        self.n_in_queue = Counter()
        self.data_aggregator = DataAggregator(self.data_q,
                                              self.batch_q,
                                              self.stop_event,
                                              batch_size,
                                              n_in_queue=self.n_in_queue)

        self.data_generators = [DataGenerator(self.data_q,
                                              self.stop_event,
                                              gpus=gpus[ii],
                                              generator_fun=generator_fun[ii])
                                for ii in range(len(gpus))]
        for w in self.data_generators:
            w.start()
        self.data_aggregator.start()

    def next_batch(self):
        self.n_in_queue.increment(-1)
        return self.batch_q.get()

    def put_batch(self, batch):
        if not self.batch_q.full():
            self.batch_q.put(batch)
            self.n_in_queue.increment(1)

    def close(self, timeout: int = 5):
        self.stop_event.set()

        for w in self.data_generators:
            w.join(timeout=timeout)
            while w.is_alive():
                w.terminate()
        self.data_aggregator.join(timeout=timeout)
        while self.data_aggregator.is_alive():
            self.data_aggregator.terminate()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()
