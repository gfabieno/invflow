#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from InvFlow.Inputqueue import DataGenerator, DataAggregator
from InvFlow.Inputqueue import BatchManager, GeneratorFunctionExample
from multiprocessing import Queue, Event
import time

def test_DataGenerator():
    stop_event = Event()
    single_task_q = Queue(10)
    gen_fun = GeneratorFunctionExample()
    sgb = DataGenerator(single_task_q,
                        stop_event,
                        1,
                        generator_fun=gen_fun.generator_fun)
    assert sgb.done_q.empty() is True
    sgb.start()
    time.sleep(2)
    stop_event.set()
    assert sgb.done_q.empty() is False
    for ii in range(0,10):
        assert sgb.done_q.get() == 1
    assert sgb.done_q.empty() is True

def test_DataAggregator():
    stop_event = Event()
    single_task_q = Queue(10)
    batch_queue = Queue(10)
    gen_fun = GeneratorFunctionExample()
    sgb = DataGenerator(single_task_q,
                        stop_event,
                        1,
                        generator_fun=gen_fun.generator_fun)
    abq = DataAggregator(single_task_q, batch_queue, stop_event, 3)
    assert abq.done_q.empty() is True
    sgb.start()
    abq.start()
    time.sleep(2)
    stop_event.set()
    assert sgb.done_q.empty() is False
    assert abq.done_q.empty() is False
    for ii in range(0,10):
        assert abq.done_q.get() == [1, 1, 1]

def test_BatchManager():
    gen_fun = GeneratorFunctionExample()
    batch_manager = BatchManager(10, 3, [1,1], gen_fun.generator_fun)
    time.sleep(2)
    assert batch_manager.data_q.empty() is False
    assert batch_manager.batch_q.empty() is False

    assert batch_manager.next_batch() == [1, 1, 1]
    batch_manager.close()