#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from InvFlow.SeisCL import SeisCL
from InvFlow.Forward import FclassError
import os
import numpy as np
import tensorflow as tf
import hdf5storage as h5mat
import pytest

from tensorflow.python.framework.ops import reset_default_graph
reset_default_graph()

def test_SeisCL_assign_filenames():
    F=SeisCL()
    file='test'
    F.file=file
    assert F.file
    assert F.file_model==file+"_model.mat"
    assert F.file_csts==file+"_csts.mat"
    assert F.file_dout==file+"_dout.mat"
    assert F.file_gout==file+"_gout.mat"
    assert F.file_rms==file+"_rms.mat"
    assert F.file_movout==file+"_movie.mat"
    assert F.file_din==file+"_din.mat"
    assert F.file_res==file+"_res.mat"

def test_SeisCL_assign_datalist():
    F=SeisCL()
    datalistfile=os.getcwd()+'/datalist.mat'
    src_pos=np.array( [[100, 200], [101, 201], [102, 202], [0,1], [1, 1] ] )
    rec_pos= np.array( [[100, 200], [101, 201], [102, 202], [0,1], [1, 2], [0,0],[0,0],[0,0] ] )
    h5mat.savemat(datalistfile, {'src_pos':src_pos,'rec_pos':rec_pos} , appendmat=False, format='7.3', store_python_metadata=True, truncate_existing=True)
    
    F.file_datalist=datalistfile
    assert np.all(F.src_pos_all==src_pos)
    assert np.all(F.rec_pos_all==rec_pos)
    assert np.all(F.allshotids==src_pos[3,:])
    os.remove(datalistfile)

def test_SeisCL_get_param_list():
    F=SeisCL()
    F.csts['L']=0
    F.csts['param_type']=0
    assert F.params==['vp','vs','rho']
    F.csts['param_type']=1
    assert F.params==['M','mu','rho']
    F.csts['param_type']=2
    assert F.params==['Ip','Is','rho']
    F.csts['L']=1
    assert F.params==['Ip','Is','rho', 'taup', 'taus']
    
def test_SeisCL_pos_var():
    F=SeisCL()
    with pytest.raises(FclassError):
        F.src_pos=None
    with pytest.raises(FclassError):
        F.src_pos=np.zeros([1,1])
    F.src_pos=np.zeros([5,1])+1
    assert np.all(F.csts['src_pos']==(np.zeros([5,1])+1))
    
    with pytest.raises(FclassError):
        F.rec_pos=None
    with pytest.raises(FclassError):
        F.rec_pos=np.zeros([1,1])
    F.rec_pos=np.zeros([8,1])+1
    assert np.all(F.csts['rec_pos']==(np.zeros([8,1])+1))
    
    F.csts['NT']=15
    with pytest.raises(FclassError):
        F.src=None
    with pytest.raises(FclassError):
        F.src=np.zeros([1,1])
    F.src=np.zeros([F.csts['NT'],1])+1
    assert np.all(F.csts['src']==(np.zeros([F.csts['NT'],1])+1))
    
def test_SeisCL_set_job():    
    F=SeisCL()
    dataid=[1]
    F.csts['L']=0
    m={'vp':np.zeros([10,1,10])+1, 'vs':np.zeros([10,1,10])+2, 'rho':np.zeros([10,1,10])+3}
    F.src_pos=np.zeros([5,1])+1
    F.rec_pos=np.zeros([8,1])+1
    F.csts['NT']=15
    F.src=np.zeros([F.csts['NT'],1])+1
    
    with pytest.raises(FclassError):
        F.set_job(dataid, m, os.getcwd() )
        
    datalistfile=os.getcwd()+'/datalist.mat'
    src_pos=np.array( [[100, 200], [101, 201], [102, 202], [0,1], [1, 1] ] )
    rec_pos= np.array( [[100, 200], [101, 201], [102, 202], [0,1], [1, 2], [0,0],[0,0],[0,0] ] )
    h5mat.savemat(datalistfile, {'src_pos':src_pos,'rec_pos':rec_pos} , appendmat=False, format='7.3', store_python_metadata=True, truncate_existing=True)
    F.file_datalist=datalistfile


    F.set_job(dataid, m, os.getcwd()+'/' )
    
    assert os.path.isfile(F.file_csts)
    mat = h5mat.loadmat(F.file_csts)
    for var in mat:
        assert np.all(F.csts[var]==mat[var])
    assert np.all(F.csts['src_pos']==src_pos[:,dataid]    )
    assert np.all(F.csts['rec_pos']==rec_pos[:,dataid]    )
        
    assert os.path.isfile(F.file_model)
    mat = h5mat.loadmat(F.file_model)
    for var in m:
        assert np.all(m[var]==mat[var])   
    
    os.remove(datalistfile)
    os.remove(F.file_csts)
    os.remove(F.file_model)

def test_SeisCL_read_data(): 
    F=SeisCL()
    with pytest.raises(FclassError):
        F.read_data(os.getcwd()+'/')
    h5mat.savemat(F.file_dout, {'vxout':np.array([0]),'vzout':np.array([1])} , appendmat=False, format='7.3', store_python_metadata=True, truncate_existing=True)
    data = F.read_data(os.getcwd()+'/')
    assert np.all(data[0]==np.array([0]))
    assert np.all(data[1]==np.array([1]))
    os.remove(F.file_dout)

def test_SeisCL_read_grad(): 
    F=SeisCL()
    with pytest.raises(FclassError):
        F.read_grad(os.getcwd()+'/', ['vp','vs','rho'])
    h5mat.savemat(F.file_gout, {'gradvp':np.array([0]),'gradvs':np.array([1]),'gradrho':np.array([2])} , appendmat=False, format='7.3', store_python_metadata=True, truncate_existing=True)
    grad = F.read_grad(os.getcwd()+'/', ['vp','vs','rho'])
    assert np.all(grad[0]==np.array([0]))
    assert np.all(grad[1]==np.array([1]))
    assert np.all(grad[2]==np.array([2]))
    os.remove(F.file_gout)

def test_SeisCL_read_Hessian(): 
    F=SeisCL()
    with pytest.raises(FclassError):
        F.read_Hessian(os.getcwd()+'/', ['vp','vs','rho'])
    h5mat.savemat(F.file_gout, {'Hvp':np.array([0]),'Hvs':np.array([1]),'Hrho':np.array([2])} , appendmat=False, format='7.3', store_python_metadata=True, truncate_existing=True)
    H = F.read_Hessian(os.getcwd()+'/', ['vp','vs','rho'])
    assert np.all(H[0]==np.array([0]))
    assert np.all(H[1]==np.array([1]))
    assert np.all(H[2]==np.array([2]))
    os.remove(F.file_gout)
    
def test_SeisCL_read_rms(): 
    F=SeisCL()
    with pytest.raises(FclassError):
        F.read_rms(os.getcwd()+'/')
    h5mat.savemat(F.file_rms, {'rms':np.array([0]),'rms_norm':np.array([1])} , appendmat=False, format='7.3', store_python_metadata=True, truncate_existing=True)
    rms = F.read_rms(os.getcwd()+'/')
    assert np.all(rms[0]==np.array([0]))
    assert np.all(rms[1]==np.array([1]))
    os.remove(F.file_rms)  
#
#def test_SeisCL_create_op():
#    dataid=tf.placeholder(dtype=tf.int16)
#    m=[tf.get_variable(name='var1', initializer=np.zeros([2,2])+1),
#       tf.get_variable(name='var2', initializer=np.zeros([2,2])+2)
#       ]
#    F=SeisCL()
#    op1= F.op(dataid,m)
#    op2= F.op(dataid,m)
#    assert op1[0].name != op2[0].name
    
