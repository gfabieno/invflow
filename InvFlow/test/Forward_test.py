#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from InvFlow.Forward import Subprocess_op, Forward
import os.path
import numpy as np
import tensorflow as tf
import pytest
from tensorflow.python.framework.ops import reset_default_graph

def test_Subprocess_op_wordir_create_delete():
    reset_default_graph()
    thisop=Subprocess_op(None,None,None)
    wrkdir=thisop.workdir
    assert os.path.exists(wrkdir)

    del thisop
    
    assert not os.path.exists(wrkdir)

def test_Forward_create_op():
    reset_default_graph()
    dataid=tf.placeholder(dtype=tf.int16)
    m=[tf.get_variable(name='var1', initializer=np.zeros([2,2])+1),
       tf.get_variable(name='var2', initializer=np.zeros([2,2])+2)
       ]
    F=Forward()
    with pytest.raises(NotImplementedError):
        op= F.op(dataid,m).op
    
#def test_Forward_op_error():
#    reset_default_graph()
#    dataid=tf.placeholder(dtype=tf.int16)
#    m=[tf.get_variable(name='var1', initializer=np.zeros([2,2])+1),
#       tf.get_variable(name='var2', initializer=np.zeros([2,2])+2)
#       ]
#    F=Forward()
#    F.callcmd=lambda x:'wrong_cmd'
#    F.set_job=lambda x,y,z:None
#    op= F.op(dataid,m).op
#    with tf.Session() as sess:
#        tf.global_variables_initializer().run()
#        with pytest.raises(tf.errors.AbortedError):
#            sess.run(op, feed_dict={dataid:[1]})
