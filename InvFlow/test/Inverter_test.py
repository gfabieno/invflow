#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from InvFlow.Inverter import Inverter
import numpy as np
import tensorflow as tf
import pytest

from tensorflow.python.framework.ops import reset_default_graph


@pytest.fixture()
def inv():
    reset_default_graph()
    x = tf.get_variable(name='vp', initializer=np.zeros([3,3])+1)
    y = 2*x+1
    costfun = tf.losses.mean_squared_error( 0, 
                                            tf.reduce_sum(y), 
                                            weights=1.0 )
    opt = tf.train.GradientDescentOptimizer(1.0) 
    bnds = [tf.constant(np.array( [[1,2], [1,2]]  ))]
    filtscales = [0]
    return Inverter(opt, costfun, x, lbfgs=2, bnds=bnds, filtscales=filtscales)


def test_costfun(inv):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        assert inv.costfun.eval()==0
        sess.run(inv.calc_cost)
        assert inv.costfun.eval()==729
        
def test_grad(inv):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        grads = sess.run(inv.grads)
        assert np.all(grads[0][0]==(np.zeros([3,3])+1))
        assert np.all(grads[0][1]==(np.zeros([3,3])+1))
        sess.run(inv.calc_grad)
        grads = sess.run(inv.grads)
        assert np.all(grads[0][0]==(np.zeros([3,3])+108))
        assert np.all(grads[0][1]==(np.zeros([3,3])+1))
        
def test_updir(inv):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        updirs = sess.run(inv.updirs)
        assert np.all(updirs[0][0]==(np.zeros([3,3])))
        assert np.all(updirs[0][1]==(np.zeros([3,3])+1))
        sess.run(inv.calc_grad)
        sess.run(inv.set_updir)
        updirs = sess.run(inv.updirs)
        assert np.all(updirs[0][0]==(np.zeros([3,3])+108))
        assert np.all(updirs[0][1]==(np.zeros([3,3])+1))
        
def test_scale(inv):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        scales = sess.run(inv.scales)
        assert scales==[0]
        sess.run(inv.calc_grad)
        sess.run(inv.set_updir)
        sess.run(inv.calc_scales)
        scales = sess.run(inv.scales)
        assert scales==[1/108*0.001]
        
def test_condition(inv):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        sess.run(inv.calc_grad)
        sess.run(inv.set_updir)
        sess.run(inv.calc_scales)
        sess.run(inv.scale_updir)
        output=np.array([   [0,0,0],
                            [0,0.001,0],
                            [0,0,0]
                        ])
        updirs = sess.run(inv.updirs)
        assert np.all(updirs[0][0]-output<1e-9)

def test_apgr(inv):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        sess.run(inv.calc_grad)
        sess.run(inv.set_updir)
        sess.run(inv.calc_scales)
        sess.run(inv.scale_updir)
        sess.run(inv.apgr)
        output=1-np.array([   [0,0,0],
                            [0,0.001,0],
                            [0,0,0]
                        ])
        updirs = sess.run(inv.updirs)
        assert np.all(updirs[0][1]-output<1e-9)
        
def test_apgr_cl(inv):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        sess.run(inv.calc_grad)
        sess.run(inv.set_updir)
        sess.run(inv.calc_scales)
        sess.run(inv.scale_updir)
        sess.run(inv.apgr_cl)
        output=1+np.array([   [0,0,0],
                            [0,0.001,0],
                            [0,0,0]
                        ])
        updirs = sess.run(inv.updirs)
        assert np.all(updirs[0][1]-output<1e-9)
        
def test_apgr_new(inv):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        sess.run(inv.calc_grad)
        sess.run(inv.set_updir)
        sess.run(inv.calc_scales)
        sess.run(inv.scale_updir)
        sess.run(inv.apgr_new, feed_dict={inv.stepin: 3})
        output=(1-np.array([   [0,0,0],
                            [0,0.002,0],
                            [0,0,0]
                        ]))
        updirs = sess.run(inv.updirs)
        assert (np.all(updirs[0][1]-output<1e-9))
        
        assert inv.step.eval()==3
        
def test_set_y(inv):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        sess.run(inv.calc_grad)
        output=np.array([ [0,0,0],
                            [0,0,0],
                            [0,0,0]
                        ])
        assert np.all( np.abs(inv.y[0][0].eval() -output)<1e-9)
        assert np.all( np.abs(inv.y[1][0].eval() -output)<1e-9)
        sess.run(inv.set_y1[0])
        assert np.all( np.abs(inv.y[0][0].eval()+inv.grads[0][0].eval())<1e-9)
        assert np.all( np.abs(inv.y[1][0].eval()-output)<1e-9)
        sess.run(inv.set_y2[0])
        assert np.all( np.abs(inv.y[0][0].eval()-output)<1e-9)
        assert np.all( np.abs(inv.y[1][0].eval()-output)<1e-9)
        
        sess.run(inv.set_y1[1])
        assert np.all( np.abs(inv.y[1][0].eval()+inv.grads[0][0].eval())<1e-9)
        assert np.all( np.abs(inv.y[0][0].eval()-output)<1e-9)
        sess.run(inv.set_y2[1])
        assert np.all( np.abs(inv.y[0][0].eval()-output)<1e-9)
        assert np.all( np.abs(inv.y[1][0].eval()-output)<1e-9)
        
def test_set_s(inv):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        sess.run(inv.calc_grad)
        sess.run(inv.set_updir)
        sess.run(inv.calc_scales)
        sess.run(inv.scale_updir)
        output=np.array([ [0,0,0],
                            [0,0,0],
                            [0,0,0]
                        ])
        assert np.all( np.abs(inv.s[0][0].eval() -output)<1e-9)
        assert np.all( np.abs(inv.s[1][0].eval() -output)<1e-9)
        sess.run(inv.set_s[0])
        assert np.all( np.abs(inv.s[0][0].eval()+inv.updirs[0][0].eval())<1e-9)
        assert np.all( np.abs(inv.s[1][0].eval()-output)<1e-9)
        
        sess.run(inv.set_s[1])
        assert np.all( np.abs(inv.s[0][0].eval()+inv.updirs[0][0].eval())<1e-9)
        assert np.all( np.abs(inv.s[1][0].eval()+inv.updirs[0][0].eval())<1e-9)
        
def test_set_rho(inv):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        sess.run(inv.calc_grad)
        sess.run(inv.set_updir)
        sess.run(inv.calc_scales)
        sess.run(inv.scale_updir)

        sess.run(inv.set_s[0])
        sess.run(inv.set_y1[0])
        assert inv.rho[0].eval()<1e-9
        assert inv.rho[1].eval()<1e-9
        sess.run(inv.set_rho[0])
        assert (inv.rho[0].eval()-1/np.sum(inv.y[0][0].eval()* inv.s[0][0].eval()))<1e-9
        assert inv.rho[1].eval()<1e-9
        sess.run(inv.set_s[1])
        sess.run(inv.set_y1[1])
        sess.run(inv.set_rho[1])
        assert (inv.rho[1].eval()-1/np.sum(inv.y[1][0].eval()* inv.s[1][0].eval()))<1e-9

def test_set_alpha(inv):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        sess.run(inv.calc_grad)
        sess.run(inv.set_updir)
        sess.run(inv.calc_scales)
        sess.run(inv.scale_updir)

        sess.run(inv.set_s[0])
        sess.run(inv.set_y1[0])
        sess.run(inv.set_rho[0])
        sess.run(inv.set_alpha[0])
        output=inv.rho[0].eval()*np.sum(inv.updirs[0][0].eval()* inv.s[0][0].eval())
        assert (inv.alpha[0].eval()-output)<1e-9
        assert inv.alpha[1].eval()<1e-9
        sess.run(inv.set_s[1])
        sess.run(inv.set_y1[1])
        sess.run(inv.set_rho[1])
        sess.run(inv.set_alpha[1])
        output=inv.rho[1].eval()*np.sum(inv.updirs[0][0].eval()* inv.s[1][0].eval())
        assert (inv.alpha[1].eval()-output)<1e-9
        
def test_set_beta(inv):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        sess.run(inv.calc_grad)
        sess.run(inv.set_updir)
        sess.run(inv.calc_scales)
        sess.run(inv.scale_updir)

        sess.run(inv.set_s[0])
        sess.run(inv.set_y1[0])
        sess.run(inv.set_rho[0])
        sess.run(inv.set_beta[0])
        output=inv.rho[0].eval()*np.sum(inv.updirs[0][0].eval()* inv.y[0][0].eval())
        assert (inv.beta[0].eval()-output)<1e-9
        assert inv.beta[1].eval()<1e-9
        sess.run(inv.set_s[1])
        sess.run(inv.set_y1[1])
        sess.run(inv.set_rho[1])
        sess.run(inv.set_beta[1])
        output=inv.rho[1].eval()*np.sum(inv.updirs[0][0].eval()* inv.y[1][0].eval())
        assert (inv.beta[1].eval()-output)<1e-9
        
def test_loopa(inv):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        sess.run(inv.calc_grad)
        sess.run(inv.set_updir)
        sess.run(inv.calc_scales)
        sess.run(inv.scale_updir)

        sess.run(inv.set_s[0])
        sess.run(inv.set_y1[0])
        sess.run(inv.set_rho[0])
        sess.run(inv.set_alpha[0])
        output=inv.updirs[0][0].eval()-inv.alpha[0].eval()*inv.y[0][0].eval()
        sess.run(inv.loopa[0])
        assert np.all(inv.updirs[0][0].eval()-output)<1e-9
        
        sess.run(inv.set_s[1])
        sess.run(inv.set_y1[1])
        sess.run(inv.set_rho[1])
        sess.run(inv.set_alpha[1])
        output=inv.updirs[0][0].eval()-inv.alpha[1].eval()*inv.y[1][0].eval()
        sess.run(inv.loopa[1])
        assert np.all(inv.updirs[0][0].eval()-output)<1e-9

def test_loopb(inv):
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        sess.run(inv.calc_grad)
        sess.run(inv.set_updir)
        sess.run(inv.calc_scales)
        sess.run(inv.scale_updir)

        sess.run(inv.set_s[0])
        sess.run(inv.set_y1[0])
        sess.run(inv.set_rho[0])
        sess.run(inv.set_alpha[0])
        sess.run(inv.set_beta[0])
        output=(inv.updirs[0][0].eval()
                +(inv.beta[0].eval()-inv.alpha[0].eval())*inv.s[0][0].eval())
        sess.run(inv.loopb[0])
        assert np.all(inv.updirs[0][0].eval()-output)<1e-9
        
        sess.run(inv.set_s[1])
        sess.run(inv.set_y1[1])
        sess.run(inv.set_rho[1])
        sess.run(inv.set_alpha[1])
        sess.run(inv.set_beta[1])
        output=(inv.updirs[0][0].eval()
                +(inv.beta[1].eval()-inv.alpha[1].eval())*inv.s[1][0].eval())
        sess.run(inv.loopb[1])
        assert np.all(inv.updirs[0][0].eval()-output)<1e-9
        
