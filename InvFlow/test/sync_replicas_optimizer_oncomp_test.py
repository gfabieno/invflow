#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from InvFlow.sync_replicas_optimizer_oncomp2 import SyncReplicasOptimizer
import numpy as np
import tensorflow as tf
import pytest
import argparse
import sys
import os
import time

from tensorflow.python.ops import control_flow_ops
from tensorflow.python.framework import ops
from tensorflow.python.ops import variable_scope

import subprocess

FLAGS = None


# def test_ps_server_create_and_kill():
#
#     n = 10
#     localhosts = "".join(["localhost:"+str(2222+i)+"," for i in range(n)])[:-1]
#     pipes = []
#     for i in range(n):
#         pipes.append(subprocess.Popen("./sync_replicas_optimizer_oncomp_test.py \
#                                  --ps_hosts="+localhosts+" \
#                                  --job_name=ps --task_index="+str(i),
#                                  stdout=subprocess.PIPE,
#                                  stderr=subprocess.PIPE,
#                                  shell=True))
#
#     for i in range(n):
#         assert pipes[i].poll() is None
#         pipes[i].kill()
#         pipes[i].communicate()
#         assert pipes[i].poll() is not None
#
# def test_print_worker_id():
#
#     n = 10
#     localhosts = "".join(["localhost:"+str(2222+i)+"," for i in range(n)])[:-1]
#     pipes = []
#     for i in range(n):
#         pipes.append(subprocess.Popen("./sync_replicas_optimizer_oncomp_test.py \
#                                  --worker_hosts="+localhosts+" \
#                                  --job_name=worker --task_index="+str(i)+"\
#                                  --test_fun=print_worker_id",
#                                  stdout=subprocess.PIPE,
#                                  stderr=subprocess.PIPE,
#                                  shell=True))
#
#     for i in range(n):
#         stdout, stderr = pipes[i].communicate()
#         assert int(stdout.decode()) == i

def print_worker_id():
    sys.stdout.write(str(FLAGS.task_index))


# def test_add_worker_ids():
#
#     n = 4
#     localhosts = "".join(["localhost:"+str(2222+i)+"," for i in range(n)])[:-1]
#     pipes = []
#     for i in range(n):
#         pipes.append(subprocess.Popen("./sync_replicas_optimizer_oncomp_test.py \
#                                  --worker_hosts="+localhosts+" \
#                                  --job_name=worker --task_index="+str(i)+"\
#                                  --test_fun=add_worker_ids",
#                                  stdout=subprocess.PIPE,
#                                  stderr=subprocess.PIPE,
#                                  shell=True))
#
#     for i in range(n):
#         stdout, stderr = pipes[i].communicate()
#         assert int(stdout.decode()) == np.sum(range(n))

def add_worker_ids(server):
    with tf.device('/job:worker/task:0'):
        total = tf.Variable(0, name='total')

    local_anchor = control_flow_ops.no_op()
    with ops.colocate_with(local_anchor):
        add_to_total = tf.assign_add(total,
                                     FLAGS.task_index,
                                     use_locking=False)
    target_total = np.sum(range(len(FLAGS.worker_hosts.split(","))))

    wid = FLAGS.task_index
    is_chief = wid == 0
    with tf.train.MonitoredTrainingSession(master=server.target,
                                           is_chief=is_chief,
                                           ) as sess:
        sess.run(add_to_total)
        total_to_output = 0
        while total_to_output < target_total:
            total_to_output = total.eval(session=sess)


    sys.stdout.write(str(total_to_output))

def test_aggregate_gradients():


    n = 3
    localhosts = "".join(["localhost:"+str(2222+i)+"," for i in range(n)])[:-1]
    pspipe = subprocess.Popen("./sync_replicas_optimizer_oncomp_test.py \
                                     --worker_hosts=" + localhosts + " \
                                     --ps_hosts=localhost:2228 \
                                     --job_name=ps --task_index=0 \
                                     --test_fun=aggregate_gradients",
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                shell=True)
    pipes = []
    for i in range(n):
        pipes.append(subprocess.Popen("./sync_replicas_optimizer_oncomp_test.py \
                                 --worker_hosts="+localhosts+" \
                                 --ps_hosts=localhost:2228 \
                                 --job_name=worker --task_index="+str(i)+"\
                                 --test_fun=aggregate_gradients",
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE,
                                 shell=True))

    for i in range(n):
        stdout, stderr = pipes[i].communicate()
        a = 2+2
        #assert int(stdout.decode()) == np.sum(range(n))

def aggregate_gradients(cluster, server):

    # with tf.device(tf.train.replica_device_setter(cluster=cluster)):
    with tf.device('/job:ps/task:0'):
        x = tf.Variable(1.0, name='x', dtype=tf.float32)
        c = tf.placeholder(name='c', shape=(), dtype=tf.float32)
    y = x * c
    opt = tf.train.GradientDescentOptimizer(10.0)
    global_step = tf.train.get_or_create_global_step()
    nworker = len(FLAGS.worker_hosts.split(","))
    opt = tf.train.SyncReplicasOptimizer(opt,
                                replicas_to_aggregate=nworker,
                                total_num_replicas=nworker,
                                use_locking=False
                                )
    grad = opt.compute_gradients(y)
    grad = [(g*10,v) for g,v in grad]
    gradapp = opt.apply_gradients(grad, global_step=global_step)

    wid = FLAGS.task_index
    is_chief = (wid == 0)
    hooks = [opt.make_session_run_hook(is_chief),
             tf.train.StopAtStepHook(last_step=1)]
    with tf.train.MonitoredTrainingSession(master=server.target,
                                           is_chief=is_chief,
                                           hooks=hooks
                                           ) as sess:
        while not sess.should_stop():
            sess.run(gradapp, feed_dict={c: np.float32(wid+2)})
    with tf.Session(server.target) as sess:
        total_to_output = sess.run(x)
    sys.stdout.write(str(total_to_output))


def main(_):

    """
        ________________Create the tensforflow cluster__________________________
    """

    specs = {}
    if FLAGS.ps_hosts:
        specs["ps"] = FLAGS.ps_hosts.split(",")
    if FLAGS.worker_hosts:
        specs["worker"] = FLAGS.worker_hosts.split(",")

    # Create a cluster from the parameter server and worker hosts.
    cluster = tf.train.ClusterSpec(specs)

    # Create and start a server for the local task.
    server = tf.train.Server(cluster,
                             job_name=FLAGS.job_name,
                             task_index=FLAGS.task_index)
    if FLAGS.job_name == "ps":
        server.join()
    elif FLAGS.job_name == "worker":

        if FLAGS.test_fun == "print_worker_id":
            print_worker_id()
        elif FLAGS.test_fun == "add_worker_ids":
            add_worker_ids(server)
        elif FLAGS.test_fun == "aggregate_gradients":
            aggregate_gradients(cluster, server)
        else:
            raise ValueError("No test function called %s" % FLAGS.test_fun)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.register("type", "bool", lambda v: v.lower() == "true")
    # Flags for defining the tf.train.ClusterSpec
    parser.add_argument(
        "--ps_hosts",
        type=str,
        default=None,
        help="Comma-separated list of hostname:port pairs"
    )
    parser.add_argument(
        "--worker_hosts",
        type=str,
        default=None,
        help="Comma-separated list of hostname:port pairs"
    )
    parser.add_argument(
        "--job_name",
        type=str,
        default="",
        help="One of 'ps', 'worker'"
    )
    # Flags for defining the tf.train.Server
    parser.add_argument(
        "--task_index",
        type=int,
        default=0,
        help="Index of task within the job"
    )
    # Flags for defining the test function
    parser.add_argument(
        "--test_fun",
        type=str,
        default="",
        help="Index of task within the job"
    )
    FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)

