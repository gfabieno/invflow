# -*- coding: utf-8 -*-
"""
Interface to SeisCL for Tensorflow
"""
import hdf5storage as h5mat
import h5py as h5
import numpy as np
import math
from InvFlow.Forward import Forward, FclassError
from SeisCL import SeisCL

class SeisCL(SeisCL, Forward):
    pass