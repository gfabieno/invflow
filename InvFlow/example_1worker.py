#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  3 12:26:54 2017

@author: gabrielfabien-ouellet
"""
from InvFlow.SeisCL import SeisCL
from InvFlow.Inverter import Inverter, EnableCL, InvertError
import tensorflow as tf
import numpy as np
import os
from tensorflow.python.framework.ops import reset_default_graph
import sys

if __name__ == "__main__":
    reset_default_graph()

    batch = 3
    niter = 10
    """
        ________________Set the forward problem variables_______________________
    """
    F = SeisCL()

    F.csts['N'] = np.array([200, 150])
    F.csts['dt'] = 0.0009
    F.csts['dh'] = 10.
    F.csts['NT'] = int(round(0.7 / F.csts['dt']))
    F.csts['f0'] = 15.
    F.csts['fmax'] = 0
    F.csts['fmin'] = 0
    F.csts['L'] = 0
    F.csts['nab'] = 32
    F.csts['tmax'] = 0
    F.csts['seisout'] = 2
    F.csts['resout'] = 0
    F.csts['ND'] = 2
    F.csts['pref_device_type'] = 4
    F.file_din = os.getcwd() + '/data0_10.mat'

    """
        ________________#Add sources and receivers positions____________________
    """
    for ii in range(0, 126, 12):
        F.add_src_pos((F.csts['nab'] + 5) * F.csts['dh'],
                      0,
                      (F.csts['nab'] + 5 + ii) * F.csts['dh'],
                      ii / 12, 100)
        for jj in range(0, 126):
            F.add_rec_pos((F.csts['N'][1] - F.csts['nab'] - 5) * F.csts['dh'],
                          0,
                          (F.csts['nab'] + 5 + jj) * F.csts['dh'],
                          ii / 12)
    F.ricker()
    F.read_src = False

    """
        __________________________#Define the model_____________________________
    """
    vp0 = np.zeros((F.csts['N'][0], F.csts['N'][1])) + 4000
    vs0 = np.zeros((F.csts['N'][0], F.csts['N'][1])) + 3000
    rho0 = np.zeros((F.csts['N'][0], F.csts['N'][1])) + 2000
    #    vs0[65:95,50:100]=3100
    vp0[105:135, 50:100] = 3800
    rho0[65:95, 50:100] = 3000

    m = [tf.get_variable(name='vp', initializer=vp0),
         tf.get_variable(name='vs', initializer=vs0),
         tf.get_variable(name='rho', initializer=rho0)
         ]
    mtotrain = [m[0], m[2]]

    dataid = tf.placeholder(dtype=tf.int16, name='data_ids')

    """
        __________________________#Compute true data____________________________
    """
    F.input_residuals = True
    if not os.path.exists(F.file_din):
        thisop = F.op(dataid, m, name='Forward_true')
        output = thisop.op
        with EnableCL() as sess:
            tf.global_variables_initializer().run(session=sess)
            data = sess.run(output, feed_dict={dataid: F.src_pos[3, :]})
            if F.csts['seisout'] == 1:
                F.write_data("./data0_10.mat",
                             {'vx': data[0][0], 'vz': data[0][1]})
            elif F.csts['seisout'] == 2:
                F.write_data("./data0_10.mat",
                             {'p': data[0][0]})
        del thisop

    """
        ________________Set the parameters for inversion_______________________
    """

    F.input_residuals = False
    F.read_src = True
    F.csts['back_prop_type'] = 1
    F.csts['gradfreqs'] = np.array([F.csts['f0']])
    F.csts['read_src'] = 1
    F.csts['rmsout'] = 1
    F.csts['gradout'] = 1
    F.csts['HOUT'] = 1
    F.csts['scale_datarms'] = False
    F.csts['freqstep'] = 1.
    F.csts['scalerms'] = 0
    F.csts['scalermsnorm'] = 0
    F.csts['scaleshot'] = 0
    F.file_datalist = os.getcwd() + '/data0_10.mat'
    F.csts['gradfreqs'] = np.array([F.csts['f0']])

    """
        ____________________Set the Inverter object____________________________
    """
    with tf.name_scope('Forward'):
        thisop = F.op(dataid, m, name='Forward')
        output = tf.sqrt(thisop.op[0])
        Hessian = thisop.opH
    costfun = tf.losses.mean_squared_error(0, output, weights=1.0)
    opt = tf.train.GradientDescentOptimizer(10.0)

    with tf.name_scope('regularize'):
        nab = F.csts['nab']
        bnd = tf.constant(np.array([[nab + 2, -nab], [nab, -nab]]),
                          name='boundary')
        bnds = [bnd for _ in mtotrain]
        filtscale = tf.Variable(0, name='filtscale', trainable=False)
        filtscales = [filtscale for _ in mtotrain]

    inv = Inverter(opt,
                   costfun,
                   mtotrain,
                   bnds=bnds,
                   filtscales=filtscales,
                   lbfgs=5,
                   Hessian=Hessian,
                   maxmin=[[3000, 4000], [1000, 4000]],
                   accum=3)

    """
        ______________Set variables to monitor during inversion_________________
    """
    tf.summary.scalar("cost", inv.costfun)  # Monitor the cost function
    for v in mtotrain:
        im = tf.reshape(v, [1, int(v.shape[0]), int(v.shape[1]), 1])
        image = tf.cast(im, tf.float32)
        tf.summary.image(name=v._variable.name,
                         max_outputs=1,
                         tensor=image)   # Output images of model paramters
    summary_op = tf.summary.merge_all()  # These can be viewed with tensorboard

    """
        _____________________Hooks to control the inversion_____________________
    """
    hooks = [tf.train.StopAtStepHook(last_step=niter),
             tf.train.SummarySaverHook(save_steps=1, summary_op=summary_op),
             tf.train.CheckpointSaverHook(checkpoint_dir='logs',
                                          save_steps=1,
                                          saver=tf.train.Saver(max_to_keep=None)
                                          )
             ]

    starting_model = [
        m[0].assign(np.zeros((F.csts['N'][0], F.csts['N'][1])) + 4000),
        m[1].assign(np.zeros((F.csts['N'][0], F.csts['N'][1])) + 3000),
        m[2].assign(np.zeros((F.csts['N'][0], F.csts['N'][1])) + 2000)
    ]

    """
        _________________________Perform the inversion__________________________
    """


    def sesscreate():
        return tf.train.MonitoredTrainingSession(checkpoint_dir='logs',
                                                 save_checkpoint_secs=None,
                                                 hooks=hooks,
                                                 save_summaries_steps=1)


    with EnableCL(session=sesscreate) as sess:

        sess.run(starting_model)

        while not sess.should_stop():
            print('iter:%d, err:%f, step:%f, maxvp:%d' % (
                inv.global_step.eval(session=sess),
                inv.costfun.eval(session=sess),
                inv.step.eval(session=sess),
                np.max(m[0].eval(session=sess))))
            this_batch = np.random.choice(F.src_pos_all[3, :],
                                          batch,
                                          replace=False)
            try:
                inv.run(sess, feed_dict=[{dataid: [s]} for s in this_batch])
                #inv.run(sess,  feed_dict={dataid: this_batch})
            except InvertError as msg:
                print(msg)
            sys.stdout.flush()

    del thisop
